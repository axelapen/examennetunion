﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AxelApen.Startup))]
namespace AxelApen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
