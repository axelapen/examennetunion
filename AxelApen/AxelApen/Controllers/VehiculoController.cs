﻿using AxelApen.Models;
using AxelApen.Models.viewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AxelApen.Controllers
{
    public class VehiculoController : Controller
    {
        // GET: Vehiculo
        public ActionResult Index()
        {

            List<Vehiculo> listaVehiculo;
            using (dbPruebaDesarrolloEntities bd = new dbPruebaDesarrolloEntities())
            {
                listaVehiculo = (from veh in bd.VEHICULO
                                 select new Vehiculo
                                 {
                                     matricula = veh.MATRICULA,
                                     tipoVehiculo = veh.TIPO_VEHICULO,
                                     modelo = veh.MODELO,
                                     marca = veh.MARCA,
                                     linea = veh.LINEA,
                                     propietario = veh.PROPIETARIO
                                 }


                    ).ToList();
            }
            return View(listaVehiculo);
        }

        public ActionResult Nuevo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Nuevo(VehiculoViewModel model)
        {
            using (dbPruebaDesarrolloEntities db = new dbPruebaDesarrolloEntities())
            {
                var vehiculo = new VEHICULO();
                vehiculo.MATRICULA = model.matricula;
                vehiculo.TIPO_VEHICULO = model.tipoVehiculo;
                vehiculo.MODELO = model.modelo;
                vehiculo.MARCA = model.marca;
                vehiculo.LINEA = model.linea;
                vehiculo.PROPIETARIO = model.propietario;
                db.VEHICULO.Add(vehiculo);
                db.SaveChanges();

            }
            return Redirect("~/Vehiculo/");
        }

      
        public ActionResult Editar(string id)
        {
            VehiculoViewModel model = new VehiculoViewModel();

            using (dbPruebaDesarrolloEntities bd = new dbPruebaDesarrolloEntities())
            {
                var vehiculo = bd.VEHICULO.Find(id);
                model.matricula = vehiculo.MATRICULA;
                model.tipoVehiculo = vehiculo.TIPO_VEHICULO;
                model.modelo = vehiculo.MODELO;
                model.marca = vehiculo.MARCA;
                model.linea = vehiculo.LINEA;
                model.propietario = vehiculo.PROPIETARIO;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(VehiculoViewModel model)
        {
            using (dbPruebaDesarrolloEntities bd = new dbPruebaDesarrolloEntities())
            {
                var vehiculo = bd.VEHICULO.Find(model.matricula);
                vehiculo.TIPO_VEHICULO = model.tipoVehiculo;
                vehiculo.MODELO = model.modelo;
                vehiculo.MARCA = model.marca;
                vehiculo.LINEA = model.linea;
                vehiculo.PROPIETARIO = model.propietario;
                bd.Entry(vehiculo).State = System.Data.Entity.EntityState.Modified;
                bd.SaveChanges();

            }
            return Redirect("~/Vehiculo/");

        }

        public ActionResult Eliminar(string id)
        {
            using (dbPruebaDesarrolloEntities bd = new dbPruebaDesarrolloEntities())
            {
                var veh = bd.VEHICULO.Find(id);
                    bd.VEHICULO.Remove(veh);
                bd.SaveChanges();
                
            }
               return Redirect("~/Vehiculo/");
        }
    }
    }