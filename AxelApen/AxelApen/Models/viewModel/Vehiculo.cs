﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxelApen.Models.viewModel
{
    public class Vehiculo
    {
        public String matricula { get; set; }
        public String tipoVehiculo { get; set; }
        public String modelo { get; set; }
        public String marca { get; set; }
        public String linea { get; set; }
        public String propietario { get; set; }

    }
}