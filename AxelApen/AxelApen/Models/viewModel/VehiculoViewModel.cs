﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AxelApen.Models.viewModel
{
    public class VehiculoViewModel
    {
        [Required]
        [StringLength(15)]
        public String matricula { get; set; }
        [Required]
        [StringLength(2)]
        public String tipoVehiculo { get; set; }
        [Required]
        [StringLength(15)]
        public String modelo { get; set; }
        [Required]
        [StringLength(10)]
        public String marca { get; set; }
        [Required]
        [StringLength(10)]
        public String linea { get; set; }
        [Required]
        [StringLength(100)]
        public String propietario { get; set; }
    }
}